var exec = require('cordova/exec');

// Camera: front or back
// cb: function (err) { //if err is empty then execution was a success
// }
exports.startCapture = function (camera, cb) {
    function onResultOk(data) {
      cb("");
    }
    function onResultError(err) {
      cb(err);
    }
    exec(onResultOk, onResultError, 'CameraFrames', 'startCapture', [camera]);
};

exports.pause = function () {
    exec(null, null, 'CameraFrames', 'pause', []);
};

exports.resume = function () {
    exec(null, null, 'CameraFrames', 'resume', []);
};

exports.capture = function(data){
    /**
     * Just a placeholder, iOS is going to call this
     * directly when the camera stream is ready.
     *
     * Define this func in your app to use the data.
     *
     * Example:
     *
     * var imageData = document.getElementById('<imageId>')
     *
     * cordova.plugins.CameraFrames.capture = function(data){
     *      image.src = imageData
     * }
     * //Starting the stream
     * cordova.plugins.CameraFrames.startCapture();
     */
};
