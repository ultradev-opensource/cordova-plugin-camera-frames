import Foundation
import AVFoundation

struct Log: TextOutputStream {

    func write(_ string: String) {
        let fm = FileManager.default
        //let log = fm.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("log.txt")
        let paths = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)
        let documentDirectoryPath = paths.first!
        let log = documentDirectoryPath.appendingPathComponent("log.txt")
        if let handle = try? FileHandle(forWritingTo: log) {
            handle.seekToEndOfFile()
            handle.write(string.data(using: .utf8)!)
            handle.closeFile()
        } else {
            try? string.data(using: .utf8)?.write(to: log)
        }
    }
}

var logger = Log()

@objc(CameraFrames)
class CameraFrames: CDVPlugin, AVCaptureVideoDataOutputSampleBufferDelegate {
    private var position = AVCaptureDevicePosition.front
    private let myPreset = AVCaptureSessionPresetLow
    private let sessionQueue = DispatchQueue(label: "session queue")
    private let captureSession = AVCaptureSession()
    private let context = CIContext()

    private var permissionGranted = false
    private var mainCommand: CDVInvokedUrlCommand?
    private var isSendingFrames = false
    //private let sessionQueue = DispatchQueue.main//(label: "camerabase64")
    /*override init() {
        super.init()
    }*/
    func customlog(str: String) {
      print(str, Date(), to: &logger)
    }
    func errback(str: String) {
      customlog(str: str)
      var pluginResult = CDVPluginResult(
          status: CDVCommandStatus_ERROR, messageAs: str
      )
      DispatchQueue.main.async { [unowned self] in
          self.commandDelegate!.send(pluginResult, callbackId: mainCommand?.callbackId)
      }
    }

    @objc(startCapture:)
    func startCapture(command: CDVInvokedUrlCommand) {
        customlog(str: "Start capture")
        mainCommand = command
        //commandDelegate.evalJs("alert('Start capture')")
        checkPermission(command: command)
        /*sessionQueue.async { [unowned self] in
            self.configureSession(command: command)
            self.captureSession.startRunning()
        }*/
    }

    private func checkPermission(command: CDVInvokedUrlCommand) {
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            // The user has previously granted access to the camera.
            customlog(str: "Camera previously authorized")
            //commandDelegate.evalJs("alert('Camera authorized')")
            permissionGranted = true
            bootSession(command: command)
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. We suspend the session queue to delay session
             setup until the access request has completed.

             Note that audio access will be implicitly requested when we
             create an AVCaptureDeviceInput for audio during session setup.
             */
            customlog(str: "Requesting camera access")
            //commandDelegate.evalJs("alert('Requesting camera access')")
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] granted in
                if !granted {
                    self.errback(str: "Access not granted")
                    self.permissionGranted = false
                } else {
                    self.errback(str: "Access granted")
                    self.permissionGranted = true
                    self.bootSession(command: command)
                }
            })
        default:
            // The user has previously denied access.
            errback(str: "User denied camera access")
            //commandDelegate.evalJs("alert('User denied camera access')")
            permissionGranted = false
        }
    }

    func bootSession(command: CDVInvokedUrlCommand) {
      sessionQueue.async { [unowned self] in
        customlog(str: "Start async session config")
        if self.configureSession(command: command) ?? false {
          self.customlog(str: "Start Running Capture Session")
          self.resume(command: command)
          var pluginResult = CDVPluginResult(
              status: CDVCommandStatus_OK, messageAs: "Success"
          )
          self.customlog(str: "Send success callback to cordova")
          self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
        }
      }
    }

    func configureSession(command: CDVInvokedUrlCommand) -> Bool? {
        // Selecting the camera from the device
        guard permissionGranted else { errback(str: "No permission to configure session"); return false }
        captureSession.sessionPreset = myPreset
        let cameraString = command.arguments[0] as? String ?? "front"
        switch cameraString {
        case "back":
            position = AVCaptureDevicePosition.back
        default:
            position = AVCaptureDevicePosition.front
        }
        customlog(str: "Getting camera "+cameraString)
        let camera = selectCaptureDevice()
        guard let captureDeviceInput = try? AVCaptureDeviceInput(device: camera) else { errback(str: "Input couldnt be created"); return false }
        if !captureSession.canAddInput(captureDeviceInput) {
          /*customlog(str: "Input cant be added. First attempt")
          for ii in captureSession.inputs {
              captureSession.removeInput(ii as! AVCaptureInput)
          }
          customlog(str: "Clear inputs ok")
          guard captureSession.canAddInput(captureDeviceInput) else {
            errback(str: "Input cant be added")
            return false
          }*/
          errback(str: "Input cant be added")
          return false
        }

        customlog(str: "Can add input")
        captureSession.addInput(captureDeviceInput)
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: sessionQueue)
        guard captureSession.canAddOutput(videoOutput) else { errback(str: "Cant add video output"); return false }
        customlog(str: "Cand add output")
        captureSession.addOutput(videoOutput)
        customlog(str: "Creating connection")
        guard let connection = videoOutput.connection(withMediaType: AVFoundation.AVMediaTypeVideo) else { errback(str: "Couldnt make connection"); return false }
        customlog(str: "Main configurations done")
        guard connection.isVideoOrientationSupported else { customlog(str: "Video orientation unsuportted"); return true }//Not a critical problem --> return true
        guard connection.isVideoMirroringSupported else { customlog(str: "Video mirrorring unsuportted"); return true }//Not a critical problem --> return true
        connection.videoOrientation = .portrait
        connection.isVideoMirrored = (position == AVCaptureDevicePosition.front)
        return true
    }

    private func selectCaptureDevice() -> AVCaptureDevice? {
        return AVCaptureDevice.devices().filter {
            ($0 as AnyObject).hasMediaType(AVMediaTypeVideo) &&
                ($0 as AnyObject).position == position
            }.first as? AVCaptureDevice
    }

    @objc(pause:)
    func pause(command: CDVInvokedUrlCommand){
        if (captureSession.isRunning) {
          customlog(str: "Stoping")
          captureSession.stopRunning()
        } else {
          customlog(str: "Session already stopped")
        }
    }

    @objc(resume:)
    func resume(command: CDVInvokedUrlCommand) {
        if (captureSession.isRunning) {
          customlog(str: "Session already running")
          return
        }
        customlog(str: "Running")
        captureSession.startRunning()
    }

    // MARK: Sample buffer to UIImage conversion
    private func imageFromSampleBuffer(sampleBuffer: CMSampleBuffer) -> UIImage? {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return nil }
        let ciImage = CIImage(cvPixelBuffer: imageBuffer)
        guard let cgImage = context.createCGImage(ciImage, from: ciImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }

    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
      autoreleasepool{
        guard let uiImage = imageFromSampleBuffer(sampleBuffer: sampleBuffer) else { customlog(str: "Couldnt read sample buffer"); return }
        let imageData = UIImageJPEGRepresentation(uiImage, 0.3)
        // Generating a base64 string for cordova's consumption
        let base64 = imageData?.base64EncodedString(options: Data.Base64EncodingOptions.endLineWithLineFeed)
        // Describe the function that is going to be call by the webView frame
        let javascript = "cordova.plugins.CameraFrames.capture('data:image/jpeg;base64,\(base64!)')"
        DispatchQueue.main.async { [unowned self] in
            //Main queue recommended for UI updates
            //Avoid delegate the hassle of dealing with the sessionQueue
            self.commandDelegate.evalJs(javascript)
        }
        /*if let webView = webView {
            if let uiWebView = webView as? UIWebView {
                // Evaluating the function
                if !isSendingFrames {
                  isSendingFrames = true
                  customlog(str: "Sending frames started")
                  uiWebView.stringByEvaluatingJavaScript(from: "alert('Sending frames '+'\(base64!)'.substr(0,120))")
                }
                uiWebView.stringByEvaluatingJavaScript(from: javascript)
            } else {
              customlog(str: "Could not start webview")
              if !isSendingFrames {
                isSendingFrames = true
                customlog(str: "Sending frames started")
                commandDelegate.evalJs("alert('Sending frames '+'\(base64!)'.substr(0,120))")
              }
              commandDelegate.evalJs(javascript)
            }
        } else {
            customlog(str: "Webview is nil")
            //commandDelegate.evalJs(javascript)
        }*/
        //DispatchQueue.main.async { [unowned self] in
            //self.delegate?.captured(image: uiImage)
        //}
      }
    }
}
